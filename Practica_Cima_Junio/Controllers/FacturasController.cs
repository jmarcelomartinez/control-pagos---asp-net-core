﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Practica_Cima_Junio.Models;

namespace Practica_Cima_Junio.Controllers
{
    public class FacturasController : Controller
    {
        private readonly ControlPagosContext _context;

        public FacturasController(ControlPagosContext context)
        {
            _context = context;
        }

        // GET: Facturas
        public async Task<IActionResult> Index()
        {
            var controlPagosContext = _context.Facturas.Include(f => f.IdUsuarioNavigation);
            return View(await controlPagosContext.ToListAsync());
        }

        // GET: Facturas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .Include(f => f.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.IdFactura == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // GET: Facturas/Create
        public IActionResult Create()
        {
            //  ViewData["IdUsuario"] = new SelectList(_context.AspNetUsers, "Id", "Nombre");
            ViewData["idUsuario"] = _context.AspNetUsers.ToList().Select(r => new SelectListItem
            {
                Text = r.Nombre + " " + r.Apellido,
                Value = r.Id
            });
            return View();
        }
        public IActionResult AgregarProducto(int Id)
        {
            ViewData["datosUsuario"] = _context.Facturas.Include(r => r.IdUsuarioNavigation).
                                                         FirstOrDefault(r => r.IdFactura == Id);
            ViewData["IdFactura"] = Id;
            ViewData["IdProducto"] = _context.Productos.ToList().Select(r => new SelectListItem
            {
                Text = r.NombreProducto,
                Value = r.IdProducto + ""
            });
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AgregarProducto([Bind("IdFacturaDetalle,IdFactura,IdProducto,Cantidad")] FacturaDetalles facturaDetalles)
        {
            var dataProducto = _context.Productos.First(r => r.IdProducto == facturaDetalles.IdProducto);
            var dataFactura = _context.Facturas.First(r => r.IdFactura == facturaDetalles.IdFactura);
            var subtotal = dataFactura.Subtotal + (double?)dataProducto.Precio*facturaDetalles.Cantidad;
            var total  =  0.0;
            total = (double)subtotal * 1.12;
            dataFactura.Total = total;
            dataFactura.Subtotal = subtotal;

            _context.FacturaDetalles.Add(facturaDetalles);

            _context.Facturas.Update(dataFactura);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> VerFactura(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var detallesFact = await _context.FacturaDetalles
                .Include(r => r.IdFacturaNavigation).Include(r => r.IdProductoNavigation).ToListAsync();
            var dataDetalles = from detallesfact in detallesFact
                               where detallesfact.IdFactura == id
                               select detallesfact;

            ViewData["datosFactura"] = _context.Facturas.Include(r => r.IdUsuarioNavigation).First(r => r.IdFactura == id);

            return View(dataDetalles.ToList());
        }



        // POST: Facturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdFactura,IdUsuario,Subtotal,Total")] Facturas facturas)
        {
            if (ModelState.IsValid)
            {
                facturas.Subtotal = 0;
                facturas.Total = 0;
                _context.Add(facturas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            // ViewData["IdUsuario"] = new SelectList(_context.AspNetUsers, "Id", "Id", facturas.IdUsuario);
            ViewData["idUsuario"] = _context.AspNetUsers.ToList().Select(r => new SelectListItem
            {
                Text = r.Nombre + " " + r.Apellido,
                Value = r.Id
            });
            return View(facturas);
        }

        // GET: Facturas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas.FindAsync(id);
            if (facturas == null)
            {
                return NotFound();
            }
            ViewData["IdUsuario"] = new SelectList(_context.AspNetUsers, "Id", "Id", facturas.IdUsuario);
            return View(facturas);
        }

        // POST: Facturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdFactura,IdUsuario,Subtotal,Total")] Facturas facturas)
        {
            if (id != facturas.IdFactura)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(facturas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacturasExists(facturas.IdFactura))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdUsuario"] = new SelectList(_context.AspNetUsers, "Id", "Id", facturas.IdUsuario);
            return View(facturas);
        }

        // GET: Facturas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .Include(f => f.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.IdFactura == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // POST: Facturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var facturas = await _context.Facturas.FindAsync(id);
            _context.Facturas.Remove(facturas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacturasExists(int id)
        {
            return _context.Facturas.Any(e => e.IdFactura == id);
        }
    }
}
