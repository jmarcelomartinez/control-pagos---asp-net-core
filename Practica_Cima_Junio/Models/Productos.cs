﻿using System;
using System.Collections.Generic;

namespace Practica_Cima_Junio.Models
{
    public partial class Productos
    {
        public Productos()
        {
            FacturaDetalles = new HashSet<FacturaDetalles>();
        }

        public int IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public decimal? Precio { get; set; }

        public ICollection<FacturaDetalles> FacturaDetalles { get; set; }
    }
}
