﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Practica_Cima_Junio.Models;

namespace Practica_Cima_Junio.Controllers
{
    public class FacturaDetallesController : Controller
    {
        private readonly ControlPagosContext _context;

        public FacturaDetallesController(ControlPagosContext context)
        {
            _context = context;
        }

        // GET: FacturaDetalles
        public async Task<IActionResult> Index()
        {
            var controlPagosContext = _context.FacturaDetalles.Include(f => f.IdFacturaNavigation).Include(f => f.IdProductoNavigation);
            return View(await controlPagosContext.ToListAsync());
        }

        // GET: FacturaDetalles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturaDetalles = await _context.FacturaDetalles
                .Include(f => f.IdFacturaNavigation)
                .Include(f => f.IdProductoNavigation)
                .FirstOrDefaultAsync(m => m.IdFacturaDetalle == id);
            if (facturaDetalles == null)
            {
                return NotFound();
            }

            return View(facturaDetalles);
        }

        // GET: FacturaDetalles/Create
        public IActionResult Create()
        {
            ViewData["IdFactura"] = new SelectList(_context.Facturas, "IdFactura", "IdFactura");
            ViewData["IdProducto"] = new SelectList(_context.Productos, "IdProducto", "IdProducto");
            return View();
        }

        // POST: FacturaDetalles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdFacturaDetalle,IdFactura,IdProducto,Cantidad")] FacturaDetalles facturaDetalles)
        {
            if (ModelState.IsValid)
            {
                _context.Add(facturaDetalles);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdFactura"] = new SelectList(_context.Facturas, "IdFactura", "IdFactura", facturaDetalles.IdFactura);
            ViewData["IdProducto"] = new SelectList(_context.Productos, "IdProducto", "IdProducto", facturaDetalles.IdProducto);
            return View(facturaDetalles);
        }

        // GET: FacturaDetalles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturaDetalles = await _context.FacturaDetalles.FindAsync(id);
            if (facturaDetalles == null)
            {
                return NotFound();
            }
            ViewData["IdFactura"] = new SelectList(_context.Facturas, "IdFactura", "IdFactura", facturaDetalles.IdFactura);
            ViewData["IdProducto"] = new SelectList(_context.Productos, "IdProducto", "IdProducto", facturaDetalles.IdProducto);
            return View(facturaDetalles);
        }

        // POST: FacturaDetalles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdFacturaDetalle,IdFactura,IdProducto,Cantidad")] FacturaDetalles facturaDetalles)
        {
            if (id != facturaDetalles.IdFacturaDetalle)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(facturaDetalles);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacturaDetallesExists(facturaDetalles.IdFacturaDetalle))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdFactura"] = new SelectList(_context.Facturas, "IdFactura", "IdFactura", facturaDetalles.IdFactura);
            ViewData["IdProducto"] = new SelectList(_context.Productos, "IdProducto", "IdProducto", facturaDetalles.IdProducto);
            return View(facturaDetalles);
        }

        // GET: FacturaDetalles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturaDetalles = await _context.FacturaDetalles
                .Include(f => f.IdFacturaNavigation)
                .Include(f => f.IdProductoNavigation)
                .FirstOrDefaultAsync(m => m.IdFacturaDetalle == id);
            if (facturaDetalles == null)
            {
                return NotFound();
            }

            return View(facturaDetalles);
        }

        // POST: FacturaDetalles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var facturaDetalles = await _context.FacturaDetalles.FindAsync(id);
            _context.FacturaDetalles.Remove(facturaDetalles);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacturaDetallesExists(int id)
        {
            return _context.FacturaDetalles.Any(e => e.IdFacturaDetalle == id);
        }
    }
}
