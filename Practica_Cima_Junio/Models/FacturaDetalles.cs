﻿using System;
using System.Collections.Generic;

namespace Practica_Cima_Junio.Models
{
    public partial class FacturaDetalles
    {
        public int IdFacturaDetalle { get; set; }
        public int? IdFactura { get; set; }
        public int? IdProducto { get; set; }
        public double? Cantidad { get; set; }

        public Facturas IdFacturaNavigation { get; set; }
        public Productos IdProductoNavigation { get; set; }
    }
}
