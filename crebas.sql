/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     6/11/2019 9:53:13 AM                         */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAS') and o.name = 'FK_FACTURAS_R_USUARIO_USUARIOS')
alter table FACTURAS
   drop constraint FK_FACTURAS_R_USUARIO_USUARIOS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURA_DETALLES') and o.name = 'FK_FACTURA__R_FACTURA_FACTURAS')
alter table FACTURA_DETALLES
   drop constraint FK_FACTURA__R_FACTURA_FACTURAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURA_DETALLES') and o.name = 'FK_FACTURA__R_PRODUCT_PRODUCTO')
alter table FACTURA_DETALLES
   drop constraint FK_FACTURA__R_PRODUCT_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAS')
            and   type = 'U')
   drop table FACTURAS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURA_DETALLES')
            and   type = 'U')
   drop table FACTURA_DETALLES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTOS')
            and   type = 'U')
   drop table PRODUCTOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('USUARIOS')
            and   type = 'U')
   drop table USUARIOS
go

/*==============================================================*/
/* Table: FACTURAS                                              */
/*==============================================================*/
create table FACTURAS (
   ID_FACTURA           int identity         not null,
   ID_USUARIO           nvarchar(450)        null,
   SUBTOTAL             float                null,
   TOTAL                float                null,
   constraint PK_FACTURAS primary key (ID_FACTURA)
)
go

/*==============================================================*/
/* Table: FACTURA_DETALLES                                      */
/*==============================================================*/
create table FACTURA_DETALLES (
   ID_FACTURA_DETALLE   int identity         not null,
   ID_FACTURA           int                  null,
   ID_PRODUCTO          int                  null,
   CANTIDAD             float                null,
   constraint PK_FACTURA_DETALLES primary key (ID_FACTURA_DETALLE)
)
go

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS (
   ID_PRODUCTO          int identity         not null,
   NOMBRE_PRODUCTO      varchar(20)          null,
   DESCRIPCION          varchar(20)          null,
   PRECIO               float                null,
   constraint PK_PRODUCTOS primary key (ID_PRODUCTO)
)
go

alter table FACTURAS
   add constraint FK_FACTURAS_R_USUARIO_USUARIOS foreign key (ID_USUARIO)
      references AspNetUsers(Id)
go

alter table FACTURA_DETALLES
   add constraint FK_FACTURA__R_FACTURA_FACTURAS foreign key (ID_FACTURA)
      references FACTURAS (ID_FACTURA)
go

alter table FACTURA_DETALLES
   add constraint FK_FACTURA__R_PRODUCT_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTOS (ID_PRODUCTO)
go

