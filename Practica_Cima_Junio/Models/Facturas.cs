﻿using System;
using System.Collections.Generic;

namespace Practica_Cima_Junio.Models
{
    public partial class Facturas
    {
        public Facturas()
        {
            FacturaDetalles = new HashSet<FacturaDetalles>();
        }

        public int IdFactura { get; set; }
        public string IdUsuario { get; set; }
        public double? Subtotal { get; set; }
        public double? Total { get; set; }

        public AspNetUsers IdUsuarioNavigation { get; set; }
        public ICollection<FacturaDetalles> FacturaDetalles { get; set; }
    }
}
